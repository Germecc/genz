<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('barangs', function (Blueprint $table) {
            $table->id('id_barang')->unique();
            $table->bigInteger('id_kategori')->unsigned();
            $table->string('nama_barang');
            $table->integer('harga_barang');
            $table->string('gambar');
            $table->integer('stok_barang');
        });

        Schema::table('barangs', function (Blueprint $table) {        
            $table->foreign('id_kategori')->references('id_kategori')->on('kategoris')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('barang');
    }
};
