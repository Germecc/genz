<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('detail_transaksis', function (Blueprint $table) {
            $table->id('id_detail')->unique();
            $table->foreignId('id_transaksi')->unsigned();
            $table->foreignId('id_barang')->unsigned();
            $table->date('tgl_transaksi');
            $table->integer('quantity');
            $table->integer('total_bayar');
            $table->integer('kembalian');
        });

        Schema::table('detail_transaksis', function (Blueprint $table) {        
            $table->foreign('id_transaksi')->references('id_transaksi')->on('transaksis')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_barang')->references('id_barang')->on('barangs')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('detail_transaksi');
    }
};
